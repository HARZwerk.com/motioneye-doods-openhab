# Surveillance PHP Script

Triggered a OpenHab Item over the RESTFull API. Motioneye (https://github.com/ccrisan/motioneye) detect a motion an triggered the surveillance PHP Script (doods.php).
DOODS is a GRPC/REST service that detects objects in images. It's designed to be very easy to use, run as a container and available remotely (https://github.com/snowzach/doods). If an object is detected, the result is sent to an OpenHab server for further processing.

## Requirements

* macOS / linux
* OpenHab v2.5 (maybe 3.x)
* Motioneye (https://github.com/ccrisan/motioneye)
* Doods (https://github.com/snowzach/doods)
* Docker (https://www.docker.com) 
* php (php-curl), git, curl

## Quick install

* Install Motioneye
* Install OpenHab
* Install DOODS at docker

