<?php

$doodsConfig = array(
    'cam'                   => $argv[1],
    'motion'                => $argv[3],
    'motioneye_id'          => $argv[2],
    'doods_server'          => '192.168.1.74',
    'doods_port'            => '8080',
    'doods_protokoll'       => 'http',
    'doods_detect_percent'  => '45',
    'doods_detecion_models' => array(
        'person',
        'dog',
        'cat',
        'bird'
    ),
    'motioneye_server'      => '192.168.1.74',
    'motioneye_port'        => '8765',
    'motioneye_protokoll'   => 'http',
    'openhab_server'        => '192.168.1.31',
    'openhab_port'          => '8080',
    'openhab_protokoll'     => 'http',
    'openhab_rest_path'      => 'rest/items',
    'openhab_item'          => 'motioncam'

);

if($doodsConfig['motion'] == "OFF"){

    $jsonArray = array(
        'cam'		=> $doodsConfig['cam'],
        'motion' => "OFF",
    );

    call_openhab(json_encode($jsonArray), $doodsConfig);

} elseif($doodsConfig['motion'] == "ON") {

    $image = $doodsConfig['motioneye_protokoll'] . "://" . $doodsConfig['motioneye_server'] . ":" . $doodsConfig['motioneye_port'] . "/picture/" . $doodsConfig['motioneye_id'] . "/current/";
    $imagedata = file_get_contents($image);
    $base64image = base64_encode($imagedata);

    $url = $doodsConfig['doods_protokoll'] . "://" . $doodsConfig['doods_server'] . ":" . $doodsConfig['doods_port'] . "/detect";

    $postRequest = array(
        "detector_name" => "default",
        "data" 			=> $base64image,
        "detect" => array (
            "*" => $doodsConfig['doods_detect_percent']
        )
    );

    $data_string = json_encode($postRequest, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);

    $ch=curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
        array(
            'Content-Type:application/json',
            'Content-Length: ' . strlen($data_string)
        )
    );

    $result = curl_exec($ch);
    curl_close($ch);

    $triggerArray = array();

    $result = json_decode($result);
    if(is_object($result)){
        foreach ($result->detections as $key => $value) {
            $logArray[] = array(
                'date'  => date("d.m.Y - H:i", time()),
                'trigger' => $value->label,
                'cam' => $doodsConfig['cam'],
                'motion' => "ON",
                'image' => $image
            );
            if(isset($value->label)){
                $detecionmodel = $doodsConfig['doods_detecion_models'];
                if(in_array($value->label, $detecionmodel)) {
                    $triggerArray[] = $value->label;

                }
            }
        }

        if(count($triggerArray) > 0) {
            $jsonArray['detections'] = array(
                'date'  => date("d.m.Y - H:i", time()),
                'trigger' => implode(', ', $triggerArray),
                'cam' => $doodsConfig['cam'],
                'motion' => "ON",
                'image' => $image
            );
            call_openhab(json_encode($jsonArray), $doodsConfig);
        }
        writeLog(print_r($logArray, true));
    }
}


function call_openhab($detection, $doodsConfig) {

    var_dump($doodsConfig['openhab_protokoll']);

    $url = $doodsConfig['openhab_protokoll'] . "://" . $doodsConfig['openhab_server'] . ":" . $doodsConfig['openhab_port'] . "/" . $doodsConfig['openhab_rest_path'] . "/" . $doodsConfig['openhab_item'];

    var_dump($url);

    $ch=curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $detection);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
        array(
            'Content-Type: text/plain',
            'Accept: application/json'
            //'Content-Length: ' . strlen($data_string)
        )
    );

    $result = curl_exec($ch);
    curl_close($ch);


}


function writeLog($log) {

    //Save string to log, use FILE_APPEND to append.
    file_put_contents('/etc/motioneye/motioneye-doods-openhab/logs/doods_'.date("j.n.Y").'.log', $log, FILE_APPEND);

}

